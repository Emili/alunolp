object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 201
  ClientWidth = 576
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object MemoPrincipal: TMemo
    Left = 8
    Top = 8
    Width = 225
    Height = 180
    TabOrder = 0
  end
  object MemoConsole: TMemo
    Left = 337
    Top = 8
    Width = 225
    Height = 180
    TabOrder = 1
  end
  object ButtonLength: TButton
    Left = 248
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Length'
    TabOrder = 2
    OnClick = ButtonLengthClick
  end
  object ButtonContains: TButton
    Left = 248
    Top = 39
    Width = 75
    Height = 25
    Caption = 'Contains'
    TabOrder = 3
    OnClick = ButtonContainsClick
  end
  object ButtonTrim: TButton
    Left = 248
    Top = 70
    Width = 75
    Height = 25
    Caption = 'Trim'
    TabOrder = 4
    OnClick = ButtonTrimClick
  end
  object ButtonLowerCase: TButton
    Left = 248
    Top = 101
    Width = 75
    Height = 25
    Caption = 'LowerCase'
    TabOrder = 5
    OnClick = ButtonLowerCaseClick
  end
  object ButtonUpperCase: TButton
    Left = 248
    Top = 132
    Width = 75
    Height = 25
    Caption = 'UpperCase'
    TabOrder = 6
    OnClick = ButtonUpperCaseClick
  end
  object ButtonReplace: TButton
    Left = 248
    Top = 163
    Width = 75
    Height = 25
    Caption = 'Replace'
    TabOrder = 7
    OnClick = ButtonReplaceClick
  end
end

object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 335
  ClientWidth = 619
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 25
    Top = 72
    Width = 66
    Height = 13
    Caption = 'Nome do Ator'
  end
  object Label2: TLabel
    Left = 24
    Top = 126
    Width = 67
    Height = 13
    Caption = 'Idade do Ator'
  end
  object Label3: TLabel
    Left = 24
    Top = 180
    Width = 63
    Height = 13
    Caption = 'Filme do Ator'
  end
  object ListaAtores: TListBox
    Left = 279
    Top = 91
    Width = 332
    Height = 129
    ItemHeight = 13
    TabOrder = 0
  end
  object EditNome: TEdit
    Left = 24
    Top = 91
    Width = 249
    Height = 21
    TabOrder = 1
  end
  object EditIdade: TEdit
    Left = 24
    Top = 145
    Width = 249
    Height = 21
    TabOrder = 2
  end
  object EditFilme: TEdit
    Left = 24
    Top = 199
    Width = 249
    Height = 21
    TabOrder = 3
  end
  object Button1: TButton
    Left = 8
    Top = 256
    Width = 75
    Height = 25
    Caption = 'Inserir'
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 89
    Top = 256
    Width = 75
    Height = 25
    Caption = 'Excluir'
    TabOrder = 5
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 178
    Top = 256
    Width = 75
    Height = 25
    Caption = 'Atualizar'
    TabOrder = 6
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 259
    Top = 256
    Width = 75
    Height = 25
    Caption = 'Ler Arquivo'
    TabOrder = 7
    OnClick = Button4Click
  end
end
